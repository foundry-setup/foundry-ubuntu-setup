#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

apt-get install -y libssl-dev
curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -
apt-get install -y nodejs

apt install /etc/nginx

rm -rf /etc/nginx
cp -rf nginx/* /etc/nginx/
cp foundry.service /etc/systemd/system/

systemctl daemon-reload
systemctl enable foundry
systemctl start foundry
systemctl restart nginx

cp 98-custom.conf /etc/sysctl.d/
sysctl --system